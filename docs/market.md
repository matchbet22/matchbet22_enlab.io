# Games Market

## The online games Market

The games industry, which was already bigger than the music and movie industry together before the pandemic, has even been more pumped up in this period. This growth was due to the combination of social isolation together with the quest for new ways of entertainment and social interaction. According to Accenture consultancy, the electronic games market turnover of U$300 bn a year, and in 2021, in Brazil, for example, grew more than 140%.

However, today there are a few options to repay nonprofessional players. Companies are focusing on developing games and little in alternatives to expand the online games ambient on services for the players. This way, space is given for platforms, such as, MatchBet which makes it possible to join have fun with games, as well as, financial profit within the gamer’s own community, in a healthy manner.

By the way, talking about gamer community, to have an idea, the game League of Legends has reached an amazing figure of more than 180 million of monthly active users (Source: Riot Games). The Dota 2, which is in second place, has more than 600 thousand daily active players, with a total bigger than 48 million registered users (Source: SteamDB).

Nowadays, sports channels show, with great audience, online game competitions on their schedules and there have been created streaming platforms dedicated to games, it is the case of Twitch. There, still, exists several national and world fairs which take thousands of millions of enthusiasts and gamers in each edition, like the CCXP, the Gamescon, among others. Furthermore, it reaches millions of people on broadcasted events, being the fairs, being the main games sports championships, such as the majors CS:GO and the world championships of League of Legends, to mention some examples.

The games market is very popular in all continents and it is estimated that there are, at least, 3.4 billion gamers in the world and they are spending, more and more, time and money played. Besides this, playing is becoming not only a social and community activity, but above all, a professional activity. The big companies, which have always invested in traditional sports, now are also present sponsoring online sports championships and players, such as Nike and a great part of the Brazilian banks, like Itaú and Bradesco.

It is expected in the next years, more than 400 million new gamers join in the online games industry, and the ones who are already in, are playing on consoles, mobile devices and PCs. However, where they are playing is so concerning as with whom they a playing, since 84% of the players say that the games help them connect and meet new people. During the pandemic, three quarters of the players said that a large part of their social interactions happen in a game platform.

With the explosive growth of online games and the gamer’s increase desire for social interactions, the game is not exclusively aligned with the product, but, predominantly, with the user’s experience. Consequently, the games industry is more and more becoming a service industry, where the users can have available a system that help them have a wider experience, not limiting only on playing games.

There MatchBet has the mission to unite, in one platform, fun and financial profit. Gamers use their own community to make profit, being it in a passive manner, with your friends, or in an active way on playing or organizing championships, for example. The platform becomes an excellent option in the services sector, up to now very little explored on games online.


## Our vision upon the Market

Even being an industry that exists for some time, there is even greater possibilities to undertake and use the games market itself in favor of other businesses, developing tools and platforms for several activities. MatchBet was born with this vision of opportunity. There isn’t a great exploitation in the played market among users in the games industry using a self crypto-asset to intermediate bets among the users, making it possible moments of entertainment and profit, with tools that bring an excellent experience for those who use it. As well as a possibility of an increase in your income being it through bets, invitations or appreciation of your coin/currency.

Therefore, MatchBet has not changed its vision throughout the project; we want that all gamers can use a platform that is worried in bringing entertainment with income generation; create several opportunities for those inserted in the gamer community find the best way to interact with other users, being by starting a tournament, challenging, bringing users to the platform, etc.. However, that each one can find your talent for entertainment and generating income. And, by doing so, that everyone contributes for a healthy, entertaining and harmonic ambient collaborating for the valuing of the coin/currency and the growth of excellent services in the gamer universe, connecting even more the gamers.

