# Roadmap
## First Quarter (Done)
- Private Investment
- Acquisition of web domains and on social media
- Definition of official name and visual identity of the brand
- Definition of rules and general structure
- Definition of usability of NFTs and associated Social Projects
- Elaboration of Matchcoin aspects
- Programming of main features on backend – see Technical Documentation [Documentação Técnica](https://app.swaggerhub.com/apis/StormGroup1/MatchBet/1.0).

## Second Quarter
- Launching of MatchBet.io official website
- Elaboration of Whitepaper and Roadmap
- Press Release texts
- Draft of contracts (smart contract)
- Matchcoin Presale 
- ICO opening
- MatchBet DAPP Listing (ex. Pancake Swap)
- Flux designation
- Approval of UX screens
- Development of all screens
- Development of the Financial Structure
- Development of the invitation and subsidy system
- Development of the automatic email sending

## Third Quarter
- Applying the Blockchain technology
- NFTs design
- Development of NFTs store
- Launch the NFTs store
- Development of Admin
- Integration and delivery of Duels with friends without validation
- Integration and delivery of the Tournament without validation

## Fourth Quarter
- Integration of validation routines in the Duels with friends and Tournaments
- Integration and enabling the Ranking
- Delivery of the Duels with Friends and Tournaments with validation
- Delivery of the Random Duels
- Delivery of the adding Games by the Users
- Developing of Partnerships System
- Developing of Open Tournaments
