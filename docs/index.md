# MatchBet
MatchBet has been developed, firstly, to democratize the online games market. The concept of the platform is to intermediate bets of skill competitive online games, among users or users team, where they can only bet among them, on games widely spreaded in the games world market, external to blockchain, such as: FPS, Battle Royale and MOBA, like LOL CSGO, DOTA2 PUBG, etc., using our crypto-asset, the Matchcoin ($MATCH). And, in a short time, games included by MatchBet and others that the users will be able to include in their profiles.

These matches among users can be done on console games or on games for PC and, in the future, on the mobile platform.

This way, for it to work perfectly, MatchBet has developed a unique victory recognizing system in the market, registered, that acts in real time, with two verification steps. Besides it, several mechanisms, which practically makes it impossible for any type of fraud. Therefore, dynamic and very safe.

MatchBet is not a project, which has been created in the last days or in the last months. It is a project that has been being designed for more than 3 years. In this period, with several meetings, schemes, drafts, ideas and decisions, we have had a big private investment, which has made the programming possible of practically all the platform structural part. This is why the delivery of different tools could be done in a short period of time. As it can be checked out on the technical documentation.

The MatchBet platform features a differential, when compared to other crypto-asset projects, because its main target is not just be profitable, but also, sustainable in the long run, once its coin, the Matchcoin, will be backed with a continuous capital flow, from investors as well as by gamers. 

For this, it has been developed an incentive strategy for new users to join in the game, invitation marketing and tournaments created by our members, with a business rule advantageous for all (investor, gamer and platform).

In this sense, the NFTs (Non-Fungible Tokens) designed in the project features in its most part, an important value for the users, because they can increase your income generation capacity in the platform and, also, a profit share participation from MatchBet.

The users have a friendly game control environment, as well as, your list of friends and in all the matches, there is interaction through chats. Besides this, they can create and participate in tournaments, check out your position in the ranking, see your NFTs inventory, visit the store, among other several features.

The users have wide access to your games and tournaments history and to your balance sheet, and the ones that have Matchcoins will have the privilege to participate in several decisions for the MatchBet platform direction, in off-chain voting on the homepage, a concept taken from the fans tokens, collaborating for a connected evolution to the expectations of our users.
