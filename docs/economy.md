# Tokenomics of Matchcoin
Matchcoin ($MATCH) is the crypto-asset that will be used for all transactions on MatchBet’s website. It is the essential component of all the financial structure of the platform and, externally, it is the asset that is susceptible to variations in the market and can be transacted. Matchcoin will be listed in the major crypto-asset brokerages worldwide.
## Presale
MatchBet will provide 20% of its Matchcoins to be acquired at Presale stages. They will be distributed as follows:

<img width="800" src="/images/prevenda_11-04-2022.jpg" alt="">

Evidently, taking into consideration the price of the currency/coin alone, PrivateSale is where one can find Matchcoins for the most attractive price. However, the percentage rate available for investors is smaller.
## Detailed Presale Board and Matchcoins Allocation:
<img width="800" src="/images/matchbet-tokenomics-18-07-2022.jpeg" alt="">

## Public Sale/TGE
<img width="800" src="/images/publicsale_23-03-2022.jpg" alt="">

Important observation: In case all the Matchcoins are not sold out in the Presale and ICO stages, the Matchcoins remained will be Burn. In this case we will benefit those that believe on our project, and we will not give a chance for those who keep waiting at the Exchanges. In other words, the Presale stages will be the best moment to buy at a low price, since the rest of the Matchcoins will go into the market at time-lock of a period of 4 years.

<img width="800" height="284" src="/images/tokenomicssummary.jpg" alt="">

## Release Schedule for $MATCH Token starting 22nd April 2022

<img width="800" src="/images/releaseschedule_17-03-2022.png" alt="">

## Airdrop

Link to AIRDROP BOT: https://hi.switchy.io/8dcf

You can join our launch inviting friends, accomplishing task and winning for each task accomplished.

Information: 

<ul>
<li>Pool PRE-ICO: 20,000,000 $Matchcoin; </li>
<li>Pool Pós-ICO: 80,000,000 $Matchcoin;</li>
<li>Registration activity: 5 $Matchcoin per user; </li>
<li>Inviting activity: 30 $Matchcoin per user; </li>
<li>Top 1,000 registration users: 5,600,000 $Matchcoin;</li>
<li>End of the PRE-ICO campaign: October 20, 2022;</li>
<li>Distribution of the PRE-ICO campaign: November 15, 2022;</li>
</ul>

Airdrop consists of handing utility tokens prizes for those who participate at MatchBet community and accomplish social task in favor of the product. The strategy of Airdrop is as a resource in the marketing actions field.

The maximum total destined to this mechanics is 1% of the tokens. In case the total amount of tokes to be distributed exceeds this amount, the individual amount destined to each user will reduce progressively and proportionally. In case tokens are left, the value will return to the company’s marketing wallet. We reserve the free use right in future Airdrops or other actions.

### Rules
1.	Participating in the Telegram community, the users will a value in tokens as prize.
2.	It is planned to invest an initial value in tokens of approximately 2000 Matchcoins (~50BUSD) as prize simply for participating at Airdrop.
3.	At each accomplished task, it will be added an extra value in your account. The tasks maybe indicated by other users or by participating in other indicated channels.
4.	At the end of the Airdrop period, the company will deliver via airdrop the actual balance of tokens directly to the user’s wallet registered at BOT.
5.	It is not necessary the KYC to receive the subsidized tokens.
6.	The ones you invite, must participate in all airdrop tasks to be counted as a valid invitation.
7.	The quantity of people invited by someone is unlimited.
8.	Multiple or false account will not be allowed and will be eliminated.
9.	Spamming in the Telegram group is not allowed and it will not be tolerated.
10. If your data sent is incorrect, you may send it again before the airdrop ends.
11. You must continue following the tasks you participated until airdrop distribution is concluded.


Bonification rules for MatchBet promoters, according to your position in the ranking: 
- From 1st to 10th = 200.000 Matchcoins each
- From 11th to 100th = 20,000 $ Matchcoins each
- From 101th to 1000th = 2,000 $ Matchcoins each 

To clear out any questions get in touch, through our channel in Telegram:
https://t.me/matchbetprevenda

## Fees and Temp-Bans
The company reserves the right to apply fees in the transactions and in the contract the does not permit fees to exceed 10%.

### Reflection fee
- The value of all transaction is removed and distributed proportionally to all the holders. [Reference](https://www.axi.com/int/blog/education/blockchain/reflection-tokens)

### Staking fee
- A proportion of fees are removed to pay staking incentives in a customized manner. The company reserves the right to describe how the staking incentives will be in a future contract.

### Ecosystem fee (community fund)
- The fee that the company receives to sustain and maintain its operations in the long term. Being able to be used discretionarily by the company.

### Burn fee
- A fee that causes coin shortage. A portion of the tokes are burnt causing deflation a benefiting holders that don’t transact frequently.

### Liquidity fee
Part of the fees will be used to add liquidity to the pool.

Half of the liquidity fee tokens will be sold to the market at DEX and converted into another element of DexPair. At this moment, 50% of the original fee lies converter into another element from par and 50% stays at the original token.

It is added liquidity at DEX using the values above.

The company can choose to be the holder of the deposit receipts at DEX or return them to the clients as cashback, encouraging them to do staking at DEX and receive dividends directly at DEX.

### AntiBot TimeLock DEX
- To avoid existing bots, and discourage acting, each wallet after an operation with dex will be temporally banned. The maximum limit allowed on contract applied by the company is 1 day.

### Circuit-Breaker fee
The circuit breaker aims to avoid despejo de valores at DEX, by this avoiding that the token devalues quickly. The mechanism consists on adding extra reflection fee, which progressively will act from a collapse initial target. From then on the bigger the decline the higher the fees. The extra fees start at 0% until it makes the sale unfeasible.

Ex. On a moveable window one day, the minimum collapse value that triggers the Circuit-Breaker fees is 10% (extra fee adding 0%) and the maximum is 20% (extra fee adding 100% effectively blocking sales transactions). After reaching the minimum, in all sales transactions there will be a reflection extra fee with a linear growth from 0% until it collapses 10%, up to 100% of fees when it gets to a 20% collapse.

