# Gamer

## Gamer and investor

The ideal gamer that we look for, to use MatchBet, is that person, above 18, that is interested in blockchain technology and NFTs; with no or very little basic knowledge on crypto-asset market, since there is the need to acquire a crypto-asset to be able to join in the platform and all the purchases and incomes are done on Matchcoin, the platform teaches through tutorials and videos how the user who is not familiar with the crypto-asset market and easily join in this new market. Besides this, the gamer that it is inserted in the electronic games community, in other words, a gamer that has several friends and/or acquaintances that also play online and that have interest in generating income parallel to your entertainment.

In addition to the ideal gamer, there is the investor; that person above 18 years old, that sees the potential of the platform and that notices the opportunity to acquire an asset, which tends to value in short, mid and long term; taking in account being it based onto a solid project, where in most part of its development it has been done by private investors and has been developed by a team led by an experienced programmer, who has an impeccable curriculum, and has worked in the major critic systems development agencies in the world, such as NASA and the European Space Agency.

