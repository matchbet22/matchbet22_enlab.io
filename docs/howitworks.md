# How MatchBet works
Matchbet, as we have seen, is a responsive site that has as its fundamental premises mediating bets, among users or user teams, on skill online games, out of the blockchain environment, using its own crypto-asset, the Matchcoin.

The site is responsive to adequate its visualization to the size of the screen on which is being shown, bearing in mind the great diversity of equipments used to access the internet (such as: computers, tablets and mobile phones). Although being founded on the games among users so that the winner gets the Matchcoins betted, it has several other features and mechanics as follows:

## Dynamic of Matchbet
At MatchBet, all users can indicate people, bet with friends from you list or with any other user in the world. Being it, through direct bet, or even starting tournaments! To maintain of the system working, cover operational costs and guaranteed benefits to the users, MatchBet keeps a percentage (rake) of 10% of the value of the prizes in all games played. However, the users may acquire NFTs from the platform to get access to several bonuses! Such as, considerable on the rake value in different bets and a lot of cashbacks!

The NFTs are a type of certificate of authenticity for digital goods established through blockchain. The NFT is a cryptographic token, which represents something unique, indivisible and exclusive and its authenticity can be confirmed and never changed.

MatchBet NFTs will have a determined validity date. All information will be contained on the description of each NFT. MatchBet may, even, come with special NFTs editions and/or lifetime NFTs!

The NFTs can be purchased at the MatchBet Store or in the market, through platforms that negotiate NFTs. They can be freely negotiated by users in blockchain platforms that deal NFTs among the people. At MatchBet the NFTs can, even, become collecting items!!

Therefore, let us know the different NFTs at MatchBet:

### Bet among Friends and Invitations NFT (Matchbet)
With the “MatchBet” NFT activated, the user will pay a rake smaller the then regular value used in the platform in bets against users that are in his list of friends. And besides this, may start getter bonus with invitations of user that you do for MatchBet. The percentage decrease of the rake, as well as possible forms of bonus and/or limitations, besides any other pertinent information, will be defined in each NFT made available for sale through MatchBet.

### Random Bet NFT (SuperBet)
With the NFT “SuperBet” activated the user will pay a rake smaller the then regular value used in the platform in bets against users that aren’t in his list of friends. In other words, in the random bets, in which the user looks for another one with similar game characteristics and Matchbet pairs them as players for that bet. The percentage decrease of the rake, as well as possible forms of bonus and/or limitations applied, besides any other pertinent information, will be defined in each NFT made available for sale through MatchBet.

### Start a Tournament NFT (ChampBet)
At MatchBet it is possible to go beyond simple bets, being competing in tournaments with other gamers for taking the chance to take all the Matchcoins on the table, or even organizing* these tournaments!

To call the attention for the one who wants to organize a tournament and repay these people, we have elaborated the “ChampBet” NFT. With it activated, the tournament organizer receives a percentage upon the Matchcoins that were set on the table! Organize tournaments, invite gamers to participate and be repaid for that with “ChampBet”! The percentage of Matchcoins that were set on the table, as well as possible forms of bonus and/or limitations applied, besides any other pertinent information, will be defined in each NFT made available for sale through MatchBet.

*Organizers can’t bet in his own tournaments.

### Cashback NFT on Bets (Cashbet) 
As previously informed, MatchBet holds a rake in all bets made in the platform. And, with this “CashBet” NFT activated, part of the percentage returns to the users who has it. Therefore, MatchBet splits the profits with its users. The percentage is the way how cashback will be done, as well as, possible forms of bonus and/or limitations applied, besides any other pertinent information, will be defined in each NFT made available for sale through MatchBet.

### CashBack NFT on Bets done by Invited ones (Super CashBet)
When one user invites another one to join MatchBet and this invited user and fills out all the form in the platform, this user is listed on the “Users Invitation” tab from the one that invited him. When a user has a Super CashBet NFT activated, he gets a percentage of the rake of the platform in all bets done by those users on your list of invitation. Definitely, this is a Super CashBet!

The percentage is the way how cashback will be done, as well as, possible forms of bonus and/or limitations applied, besides any other pertinent information, will be defined in each NFT made available for sale through MatchBet.

### CashBack NFT when purchase NFTs from the Invited ones (SalesCash)
All NFTs purchased at a MatchBet store by one of the users on the List of Invitation, will generate a bonus that is directly credited on the user’s account that made the invitation, obviously, if this user has in his inventory “SalesCash” NFT activated. The percentage is the way how cashback will be done, as well as, possible forms of bonus and/or limitations applied, besides any other pertinent information, will be defined in each NFT made available for sale through MatchBet.

### CashBack NFT in ALL Bets in the Platform (MatchBet Partner)
The “MatchBet Partner” is the NFT that makes it possible the participation upon part of the results from MatchBet for the users that have it activated. So, in the beginning of each month, whomever has the “MatchBet Partner” NFT activated will get part of all bets done in the previous month. Incredible huh?! The percentage is the way how cashback will be done, as well as, possible forms of bonus and/or limitations applied, besides any other pertinent information, will be defined in each NFT made available for sale through MatchBet.

### Highest NFT prize by Raking (RankCash)
All duels played randomly generate ranking points for the user. These points establish their position in the rank and how their standings are among other users. With “RankCash” activated, it will be possible to get an extra percentage in the random duels. So, even if the user loses the duel, the user will recover part of his coins. The percentage is the way how cashback will be done, as well as, possible forms of bonus and/or limitations applied, besides any other pertinent information, will be defined in each NFT made available for sale through MatchBet.
### MatchBet NFT Social Project (MatchHealth)
MatchBet is aware that obesity has been increasing among children and teenagers in the last years. Numbers grow because of bad food habits associated to a sedentary way of life that mostly is motivated due to the indiscriminate use of electronic gadgets. Having this in mind, we have started MatchHealth, a NFT that all the value raised with its sales, at our store, will be fully converted into measures to combat child obesity. Besides helping this very important social cause, the user that purchases it will get 250.000 [FairPlay Points](#fairplay_points).

The users will be able to follow all initiatives from MatchBet facing child obesity through our newsletters.

## Bets Dynamics
### Games among friends
To start a game to bet Matchcoins with your friends, the user must select the game he wants to play on the homepage, if there is the option play in teams, select the team, the value you would like to put into the game and select the friend in your List of Friends.

The invitation will then be sent to your friend that will be able to accept the bet or not. In case it is a YES, the chat will open, the corresponding value will be taken from both accounts and it will begin.

At this moment, the users will play the game in an external ambient to the site and to the blockchain ambient, on the console and game they have chosen, adding the nicknames informed at your profile for that online game, always being able to talk via chat. It is the commonly procedure used by gamers to play online games.

The users at the end of the game will fill out the results. In case the results are filled out in a correct manner, the validation is not necessary. In case there is a controversial situation regarding the result, the validation steps will be triggered, with consequences to the player that informed the result that is not compatible with the reality of the game.

Consequently, the winner will take the Matchcoins correspondent to the value in the game, which will appear in your account and MatchBet will hold its rake* percentage. The game will be registered in the user’s history log and ended.

*The rake percentage charged by Matchbet is 10% over the value of Matchcoins on the table. All the costs of the platform, profits, maintenance are included and taken from the percentage defined on the NFTS for the users.

### Random Games
To start a game to using Matchcoins with users from MatchBet who are not in the user’s list of friends, the user must select the game he intends to play on the homepage, if there is the option to play in teams, select the team, the value you would like to bet and select the option “search opponent”.

The platform will search for, among the users that would like to play a game in a random manner, the ones with the same game characteristics.  Finding a compatible user, the invitation will be sent and he can accept or not it. In case the user takes the bet, the chat will open, the corresponding value will be taken from both accounts and it will begin.

At this moment, the users will play the game in an external ambient to the site and to the blockchain ambient, on the console and to the game they have chosen, adding the nicknames informed at your profile for that online game, always being able to talk via chat. It is the commonly procedure used by gamers to play online games.

The users at the end of the game will fill out the results. In case the results are filled out in a correct manner, the validation is not necessary. In case there is a controversial situation regarding the result, the validation steps will be triggered, with consequences to the player that informed the result that is not compatible with the reality of the game. Consequently, the winner will take the Matchcoins correspondent to the value in the game, which will appear in your account and MatchBet will hold its rake percentage. The game will be registered in the user’s history log and ended.

#### FairPlay Points
The FairPlay Points are points that the users accumulate for fulfilling good actions at MatchBet. In case of the random games the user will receive them through well-succeed ended games, in other words, fill out the results of the game in the most honest way, informing correctly who has won and who has lost the game.

When both users, the winner as well as the opponent who lost the game, finish successfully the random game, both will receive 10.000 FairPlay Points.
The FairPlay points are essential so that a user can bet more Matchcoins in the random games; also, so that MatchBet always tries to put together in these games, users that hold close values of accumulated FairPlay Points.

The FairPlay points that a user has can be seen by your opponent, in other words, the user can decide if he takes or not the match, based on the opponent’s FairPlay Points. Therefore, we recommend that users accumulate a lot of FairPlay Points.

And, on the other hand, if the user fills out his result incorrectly and MatchBet confirms that there has been a tentative to deceive the system, this user will lose ALL his FairPlay Points, independently of how much he has. For instance, if a user has 1 million FairPlay Points (100 matches filled out correctly) and he tries to deceive the system, only once, and this being confirmed by MatchBet, all of your points will be taken. In this case, the user will have to start all over again from the beginning with zero FairPlay Points, notwithstanding other applicable penalties.

The FairPlay Points are personal and non-transferable.

### Starting and Participating in Tournaments
The user to start his own tournament must join in the Tournament tab and select the key to start a tournament. Then, the user must fill out the basic information of the tournament, such as starting date and time, number of participants, the value each user must contribute for the final prize of the tournament, how many phases will be played per day and number of players. After that, the user must select the users or teams from the List of Friends that he intends to invite to the Tournament.

Your Friends can accept or not the invitation. If any Friend of yours refuses, another Friend can be invited in his place. The Tournament will only be available for those users or teams that have accepted the invitation and all will be able to communicate through the chat inside the room.

In case the Friend accepts, when all the participants corresponding to the number of players established by the tournament have been taken and the tournament began by the user who has started it, the value in play will be taken from your account and added to the values of other participants.

The users or teams that are participating in the tournament will be able to see all phases, as well as all the groups until the final match. They will have access to information regarding the tournament, which are easily seen through the homepage of the tournament and are automatically filled out by MatchBet.

The games happen similarly to the ones with friends, however there is no invitation or choice of value, since the value arranged has previously been deposited and the invitation to participate in the tournament accepted.

Therefore, on the date and time that the game must start, each participant will get a message on your screen and the game will be started automatically.

At this moment, the users will play a game in an external ambient to the site and to the blockchain ambient, on the console and game they have chosen, adding the nicknames informed at your profile for that online game, always being able to talk via chat. It is the commonly procedure used by gamers to play online games.

So, the users, after the game is over, will fill out the results. In case the results are filled out in a correct manner, the validation is not necessary. In case there is a controversial situation regarding the result, the validation steps will be triggered, with consequences to the player that informed the result that is not compatible with the reality of the game.

The player or team that won the game moves on to the next phase till the end of the tournament. In case he wins the final game he will be considered the champion and he will receive the values deposited in the tournament, except the value over the rake percentage sent to the one who started the tournament and the value held by MatchBet, because of its rake percentage on the games.

To participate in a tournament, you only accept the invitation sent by the user who started the tournament.

Eventually our partners or MatchBet itself can start open tournaments for users from the platform. To participate in these tournaments, you have only to click on Participate, when enrollment starts. These tournaments will come up highlighted on the Tournaments tab and a warning will be sent to the users.

## Integration with MetaMask
The players must install the extension in your browser to use the MetaMask wallet, which is the wallet indicated by Binance to do the transactions in the web environment.

The user, then, must open an account at MetaMask, so that, later, your wallet can connect to MatchBet and enable you to buy items, load your account and receive your coins in your user’s wallet.

After the user gets his Matchcoins and add them to the MetaMask wallet, you must load your user’s wallet in the site. The coins available in your user’s wallet will be the ones used for transactions in the site, it will not be used the coins that are in the MetaMask wallet. Since, the coins loaded will stay in a wallet from MatchBet, to make it easy to send coins among users, and among the platform and the users.

## Registration and Login in the platform
The new users may register at MatchBet through a registration form or through any social media. However, connecting through your social media, the users will be invited to fill out a complete form when to transact funds between your wallets and MatchBet.

The information regarding the consoles or platforms that the users use to play electronic games must be filled out at this phase, so that it is shown on your homepage all the game options available at the moment, at the ambient that the player wants to play, videogame or PC, as well as, your nicknames and Id’s, when it is needed. In another moment of evolution of the platform, there will be the option for mobile gadgets.

The users that fill out all their registration and complete the identity verification from MatchBet, they will receive 100,000 FairPlay Points.
Only players above 18 are allowed to have accounts at MatchBet, and it will be permitted only one account per user.

## Homepage
Homepage screen contains all the information and main features available in the platform for our users. It will be started when the user logs in at MatchBet. On the screen there are:

- Nickname and user’s characterization at MatchBet
- The quantity of coins available in the user’s account
- Quantity of FairPlay Points
- Notifications
- List of Friends
- Ranking
- Games and Tournaments History
- Menu: Profile, Terms and Conditions of use, Privacy Policy, Settings, Customer Service and Logout
- MatchBet Bank
- The Consoles
- The Games
- The Store
- Tournaments
- Teams
- NFTs Inventory
- Inviting Users

## Teams
The users may start your teams to play games where they are necessary. The teams stay on the Teams tab on the homepage. The user may start several teams, personalizing each one, also adding images on the team’s profile. But, at this moment, the teams act out for MatchBet, as simple users, including scoring points on the ranking, on the random games.

At another moment, we have several ideas to amplify the features from the teams and its users integration. However, nowadays, the user will participate with his team, but only the user who created it can play with it in all different forms of games.

Therefore, the team, since the games are played in an external ambient to MatchBet, possesses a usefulness to characterize the gamers that usually play with their naming, but for MatchBet they behave as if they were regular users. So, gamers that are not users from MatchBet can play in the team made by the user. MatchBet does not have this external control of whom is playing in each team. However, the bonuses of the victories and the onuses of the defeats, as well as all the incomes, will only go to the one who started the Team. 

## Games and Tournaments History
There will be a record of all the matches and tournaments played by the user featuring relevant information, such as dates, results, amount betted, opponents, match and the time in which it was played.

## List of Friends
The user will be able to invite other members of the platform to be part of their List of Friends. Those featured in a user’s List of Friends are the ones eligible to be challenged in a friendly match or to the tournaments.

The user must select the add friend option, search for the friend among registered users in MatchBet and, after selecting them in the list, click onto add. The selected user will receive an invitation to be part of the List of Friends and will be able to decline or accept it.

In case they accept the invitation, this user will be featured in the List of Friends.

## Ranking
The ranking in MatchBet uses Elo Rating’s formula to calculate the scores of the users. Only scores obtained in random matches will be shown in the user’s scoreboard, that is, matches in which the user will not choose their opponent. The opponent will be designated by the platform in a pool of users that wish to bet their Matchcoins in matches with similar features.

A user’s position in the ranking may grant them bonus and prizes offered by MatchBet regarding their performance, depending on campaigns performed in the platform; they will be previously informed to the users so that they have time to play matches that will get them closer to the top of the ranking.

## Notifications
All relevant notifications to the user will be sent along with an alert located on the homepage. All notifications are shown in chronological order, so the user is able to consult them easily.

Should the alerts not be visualized, they will remain prominently featured at the notification area so that the user won’t forget to verify the information sent by MatchBet.

## Store
MatchBet’s store can be accessed through the user’s homepage. At the store, the user will find our NFTs containing all the information concerning their use, besides the licensed products that MatchBet decides to put for sale, like clothes, mugs, caps, gamer accessories, etc. 

At the store the user will also be able to find our business partners’ products, which may be purchased using our Matchcoins.

## NFTs Inventory
All NFTs acquired by the user must be added to the user’s inventory. This inventory will be available on the homepage and will feature all the active NFTs that the user has.

The NFTs acquired at MatchBet’s store will be automatically included in their inventory. Nevertheless, the user must require the inclusion, in his NFTs inventory, of assets acquired outside the store ambient. This will be in a very simple manner.

MatchBet will recommend NFT marketplaces where the users may easily find other people that wish to buy or sell their NFTs. However, the user is free to buy or sell their assets at marketplaces of their preference.

## Inviting Users
As previously mentioned, the users will be able to invite other people to register on the platform. This is a very important feature, since the user can earn many Matchcoins by inviting others.

There will be an Inviting Users tab on the homepage, where the user will have access to a list with all their invitees signed up in MatchBet.

In that same ambient there will be an exclusive link for the user that makes it possible for MatchBet to know which user invited the newly registered one; the platform will therefore, automatically, include in the inviting users list that join in the platform using that link.

This way, the user will know who used their invitation link to access the platform and will be able to get all the benefits that might come from those invitations.

Therefore, the user that wishes to invite people to register in MatchBet should only copy the exclusive link they possess, on the Inviting Users tab, and send it to those who they see as potential users of MatchBet. Thus, by registering through that link in MatchBet, those people will be part of the invited list. Over time, the more people the user invites, the more Matchcoins the user earns.

## Menu
On the Menu, that will be on the homepage, will be listed all features the user does not use frequently, such as: Profile Information, Terms and Conditions of Use, Privacy Policies, Settings, FAQ, Support and Logout.

Profile Information is a compilation of the user’s main data, such as: name, email address, photo, number of coins, Id number, nicknames, date of birth, platforms used and their tagnames. There will also be a button for editing some information.

The Terms and Conditions of Use e the Privacy Policies will be available for consultation at any time.

The Settings button can be accessed to alter the settings of images, and audio of the platform. The FAQ can be consulted there.

It will be possible for the user to be in touch with MatchBet through the Support button. There they will be able to report any problems, send information, suggestions, complaints and compliments and have any questions related to using the website.

By clicking on Logout, the user’s session will be ended and they will have to log in again to MatchBet the next time they wish to access their account.

## MatchBet Bank
At MatchBet Bank the user has access to the features and financial information of their account. This section contains all information regarding the Matchcoins the user possesses, their transactions, losses and incomes inside MatchBet. It is where the user is connected to the financial system of the platform.

Here the user will be able to connect to his MetaMask wallet and transfer Matchcoins to their user account. They will also be able to withdraw from their account to their MetaMask wallet.
When MatchBet platform is launched, the users should acquire their Matchcoins at the exchanges, like Pancakeswap. Matchcoin ia a crypto-asset and, therefore, should be acquired the same way the other coins are acquired in the market. So, with the Matchcoins in your wallet, the user should log in the platform and unload the quantity of Matchcoins the he wants in his user’s account.

There is, also, the option of transferring Matchcoins to other users and, this way there will be information of Matchcoins received from other users. Furthermore, the users will be able to check their Matchcoins balance or examine a detailed statement containing all financial information of the account in chronological order.

At Matchbet Bank will be listed all the fees Matchbet retains in its transactions and a Support button dedicated to information regarding financial matters.

It is in this section that the users will be able to see how much bonus they have earned with their invitations and, in case the user possesses NFTs that grant them financial benefits, this is where they will know in detail how much in Matchcoins they are collecting with these benefits.

At MatchBet Bank the user will be able to see and check all their financial record, with all the inputs and outputs of Matchcoins in the account related to all the features of MatchBet, such as: bets in matches and tournaments, invitations, benefits, transfers, ranking, bonus, punishments and orders at the store.

## Validation of Wins
MatchBet uses an automatic validator of wins to identify the winner of a match. It works in real time; its unique code has already been properly registered at the competent governmental bodies, and must be installed on the user’s cell phone through App Store or Google Play Store.

When the user opens the validator on their cell phone for the first time, they must log in and connect their MatchBet account to the validator, which will identify e save the cell phone number associated to your MatchBet account.

By pointing the validator at the match’s closing screen, it will automatically identify the winner of the match, based on a registered and unprecedented screen recognition system. The matches that feature automatic validation are the ones indicated by MatchBet at your list of matches.

The user will also have the option of streaming their matches, to emphasize even more the assertiveness regarding the winner.

The validation of wins feature will only be used in case of disagreement when marking who won the match on the results table. However, this disagreement will result in consequences when it is really possible to prove who was the winner in the match. The users will not be aware whether their opponent is recording the match through a streaming platform via MatchBet. Therefore, honesty is always the best way. Dishonesty will bring about a severe loss to the opponent that confirms a result that is different from the actual one.

MatchBet will warn the users of all the potential consequences of mismarking a result of the match. And, when MatchBet, for any given reason, is not able to determine the actual winner of a match, the Matchcoins will be fully returned to the users. Nonetheless, MatchBet will monitor the involved users to make sure it is not a recurrent practice of using the platform (Blacklist).

Punishments to users that are not using MatchBet honestly may range from warnings to banning from the site, incurring the freezing of his account balance and the blocking of all the access data, including the cell phone number. The punishments will be MatchBet’s discretionary and decided on a case-by-case basis, depending on the frequency and on dishonesty level. These decisions are final and unappealable, however, during all the process the users will have the opportunity to explain themselves.

Thus, the mismarking of a result will not bring about any financial loss to MatchBet or any users, or even any bonus in trying to win your Matchcoins unduly, because your opponent will make a complaint, the match will be re-examined and the Matchcoins will, at most, be returned, with no earnings to whosoever. Nevertheless, MatchBet will start monitoring the user’s behavior in the platform and, should we verify it is a repeated behavior, MatchBet will contact that user.

Eventually, over time, MatchBet will filter out bad users from those who use the platform honestly, contributing for making a healthy environment and a great opportunity of entertainment and growth for the electronic games industry.

## Games Indication by Users
Users will be able to include games in their personal profiles in MatchBet. The bets on these games, will be placed exclusively by users from the List of Friends, including starting new tournaments. The user will fill out all the information concerning these games on the Private Games tab and, after adding them, they will be listed on the Private Games tab. The matches and tournaments in these games will start in a similar manner as the ones indicated by MatchBet. 

However, no match included by the users in these games will be validated by MatchBet and the result will be marked at the risk of the users that accept the invitation to those matches.

In these cases, if there is inconsistency regarding the result, MatchBet will retain its rake and return the remaining Matchcoins to the opponents evenly. For this reason, these games will only be made available for users featured on the List of Friend.

## Chat
All matches and tournaments feature a chat among users so that their communication is facilitated and the matches may occur in the best way possible. The chat in the matches can only be accessed by the two opponents and the chat in tournaments is in the room and all users involved in it are able to express themselves.

No type of prejudice or harassment is allowed on the chat, as well as personal offenses of any kind, or messages of pornographic or violent nature, vulgar language e defamation to faith or creed. The chat has been designed to develop the interaction among users and it must not be used as means to spread disinformation and protest.

## Partnership System
Matchbet will select some partners to work in conjunction with the platform. These partners will receive bonuses, negotiated on a case-by-case basis, on the percentage to be perceived with the invitations and tournaments. Besides that, these partners will be able to create exclusive tournaments. This results from the power of influence of these people in promoting MatchBet to new members.

Additionally, it is important to stress that nothing holds a user that stands out in MatchBet for their great potential of bringing in people to the platform, from being invited to be part of our list of partners. This is a counterpart for the effort to thrive with us.

## Voting on MatchBet’s administrative decisions
Registered users will be able to take surveys and to vote regarding some administrative decisions from MatchBet.

These voting or surveys off-chain will be available on the user’s homepage and the results will be published on newsletters from MatchBet.

Therefore, users will be able to participate, together with MatchBet, in the evolution of our website and help us improve the experience with us.

## Customer Service
Users have their service channel with MatchBet. Through our Customer Service  they will be able to contact MatchBet at any time for questions, to solve any problem related to the platform, send suggestions, complaints or give any information via our support email: [info@matchbet.io](mailto:info@matchbet.io) or via the Customer Service icon featured on your homepage menu.
