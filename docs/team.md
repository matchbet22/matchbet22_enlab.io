# Equipe

## Wanderley Abreu Junior (CEO)
<img src="/images/Equipe-Nova-5-2.png" alt="Wanderley Abreu Junior" width="200"/>

CEO of Storm Group, Mechatronics Engineering from PUC-Rio. Post-graduation from MIT - USA. Specialist in critical systems with more than 20 years of working experience at institutions such as NASA, the European Space Agency – ESA and the Canadian Space Agency. By the late 90s, working alongside Brazilian prosecutor Romero Lyra, he developed, acted and perfected the first Electronic Investigations Justice Prosecution of Brazil, where he ended up being responsible for identifying over 200 pedophiles at the first operation against online pedophilia in Brazil – Operation Cathedral. Professor and instructor at the Ciclo de Estudos de Capacitação na Prevenção e Enfrentamento das Ameaças Assimétricas (Qualification of Assymetric Threathens Prevention and Confrontation Studies Cycle) and NBQRE. He was decorated with the Tiradentes Medal by the Legislative Assembly of the State of Rio de Janeiro – 2021.

## Antonio Carlos Castanon (COO)
<img src="/images/Equipe-Nova-4-2.png" alt="Antonio Carlos Castanon" width="200"/>

Graduated in Electronic Engineering from Instituto Militar de Engenharia – IME (1989), with a Master’s degree (1999) and a Doctorate (2005) in Electronic Engineering, both from Universidade Federal do Rio de Janeiro. He has experience in projects managing, especially in the field of Communications and Information Technology (CIT), having worked for about 20 years as an advisor at Centro Tecnológico do Exército (CTEx), working in Research and Development (R&D) and coordinating/managing projects: Command and Control (C2), Electronic War (EW), RADAR systems, weapons simulators and weapons systems and safety information. Professor and coordinator in graduation and post-graduation courses, particularly Electric Engineering and Labor Safety Engineering at Universidade Estácio de Sá. Currently he is dedicated to STORM, as Operations Director, in activities and projects in the IT area.

## Luan Garcia (CTO)
<img src="/images/Equipe-Nova-3-2.png" alt="Luan Garcia" width="200"/>

Graduated in IT Management and specialized in Frontend development, Luan has 15 years of experience in managing teams, software development and video platforms. He is the current CTO at StormGroup, where he is in charge of the Technology department.

## Marcos Lima (Diretor de Pesquisa e Desenvolvimento)
<img src="/images/Equipe-Nova-2-2.png" alt="Marcos Lima" width="200"/>

Graduated in Physics from Universidade de São Paulo – USP and has been a senior programmer for over 10 years. He has worked for big companies such as Ultrafarma. He now acts as Research and Development Manager at StormGroup. Marcos Lima has broad experience in computing vision and backend programming. 

## Ana Medrado (Product Designer)
<img src="/images/Equipe-Nova-1-2.png" alt="Ana Medrado" width="200"/>

Bachelor in Information Systems, doing her Master’s in Applied Computing at Federal Tecnológica do Paraná. Broadly experienced in developing technological products, with emphasis on design and usability. Currently, she is a Product Designer at Arena.im, lecturer and digital influencer in the field of technology.

## Matheus Falcão (Coordenador do Projeto)
<img src="/images/Equipe-Nova-6-1.png" alt="Matheus Falcão" width="200"/>

Graduated in Computer Science from PUC-Rio. He was part of the first class of Apple Developer Academy, where he learned how to develop iOS applications. 

He has experience in managing projects, modeling data, mobile development and backend. He is the current CEO at Noclaf Tech, where several projects have been developed in different areas and technologies.
